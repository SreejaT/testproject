import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material';
import { MatTableDataSource } from '@angular/material/table';
import {ApicallProfileManagementService} from '../../apicall-profile-management.service'
export interface PeriodicElement {
  LISTED_PARTY_ID: any;
  CARD_DISPLAY_NAME: any;
  CARD_DISPLAY_DESC: any;
  ITEM_DIVISION_CODE: any;
  Actives: any;
  DISPLAY_CARD_ID: any;
}
const ELEMENT_DATAForm: PeriodicElement[] = [
];
const fd = new FormData();
@Component({
  selector: 'app-ecom-display-card',
  templateUrl: './ecom-display-card.component.html',
  styleUrls: ['./ecom-display-card.component.scss']
})
export class ECOMDISPLAYCARDComponent implements OnInit {
  displayedColumns: string[] = ['ID', 'LISTED_PARTY_ID', 'CARD_DISPLAY_NAME', 'CARD_DISPLAY_DESC','CardImageName','ITEM_DIVISION_CODE','Active'];
  ViwColumns: string[] = ['ListedPartys', 'CardDisplayNames', 'CardDisplayDescriptions','ItemDivisionCodes','Actives','DISPLAY_CARD_ID'];
  ELEMENT_DATA: any =[];
  dataSource: any;
  ViewSource:any;
  Viewdata: any;
  @ViewChild(MatSort) sort: MatSort;
  mCardGroup: any;
  DivisionName: any;
  Listedparty: any;
  DisplayCard : FormGroup
  Date = new Date();
  SelectedReecommendedfile: any=[];
  tempdata:any=[];
  image: any =[];
  ViewDisplay: any;
  GroupId:any;
  Imagename: any;
  selectedModuleID: any;
  enable: any;
  Alength: any;
  fileName: any;
  Displaynamecheck: any;
  eachvalidation: any;
  ID:any;
  Displayname: any;
  index: any;
  itemdivisioncode: any;
  disablebutton: any;
  
 
  constructor(private service:ApicallProfileManagementService,private fd:FormBuilder) { 
    // this.DisplayCard = fd.group({
    //   DISPLAY_CARD_ID:0,
    //   DISPLAY_CARD_GROUP_ID:[],
    //   CARD_DISPLAY_NAME:[],
    //   CARD_DISPLAY_DESC:[],
    //   CARD_IMAGE_NAME:[],
    //   LISTED_PARTY_ID:[],
    //   ITEM_DIVISION_CODE:[],
    //   CREATED_BY:1001,
    //   CREATION_DATE:this.Date,
    //   LAST_UPDATED_BY:[],
    //   LAST_UPDATE_DATE:[],
    //   LAST_UPDATE_LOGIN:106,
    //   Status:[]

    // })
  }
  
  ngOnInit() {
    this.GroupId = 0;
    console.log(this.GroupId);
    
    this.fileName = null;
    this.enable = false
    // this.image[0] = "../../../../assets//ChooseImg.png"
    this.Viewdata = null;
    
    this.ELEMENT_DATA=[{ID:0,DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:0
  ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"}];
    this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
    this.service.getResultByStringValue('/api/GenericCtl/LOOKUP_MASTER', 'ECOM_CARD_GROUP').subscribe(data => { 
       this.mCardGroup = data;
       
       
      });
      this.service.getResultByStringValue('/api/GenericCtl/LOOKUP_MASTER', 'ITEM_DIVISION_CODE').subscribe(data => { 
        this.DivisionName = data;
       
        
       });
       this.service.getResultByStringValue('/api/GenericCtl/LOOKUP_MASTER', 'LISTED_PARTY_NAME').subscribe(data => { 
        this.Listedparty = data;
        
       });
       this.service.ViewDisplayCard().subscribe(res=>{
         
         this.ViewDisplay = res
         
         this.ViewSource = new MatTableDataSource<PeriodicElement>(this.ViewDisplay);
         
       })
  }
  // ngOnChanges(change){
    
    
  // }
  addrow(){
    this.Alength = this.ELEMENT_DATA.length - 1;
    console.log(this.Alength);
    
    console.log(this.ELEMENT_DATA);
    console.log(this.ELEMENT_DATA[this.Alength]);
    console.log(this.ELEMENT_DATA[0].DISPLAY_CARD_ID);

    
    // this.ELEMENT_DATA[this.Alength].Listed_Party == "") ||
    if(this.ELEMENT_DATA[0].DISPLAY_CARD_ID == 0  ){
  if( (this.ELEMENT_DATA[0].CARD_DISPLAY_NAME == "")
  || (this.ELEMENT_DATA[0].CARD_DISPLAY_DESC =="") || (this.ELEMENT_DATA[0].ITEM_DIVISION_CODE == "") ||
  (this.ELEMENT_DATA[0].LISTED_PARTY_ID == 0) || (this.ELEMENT_DATA[0].CARD_IMAGE_NAME == "") || (this.fileName == null)){
    this.service.Notificaion('please check data', 'Okey')
  }
  else{
    this.ELEMENT_DATA.unshift({DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:0
    ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"});
    
    // if(this.ELEMENT_DATA.length > 1)
    // {
    
    //   this.image[this.ELEMENT_DATA.length - 1] = "../../../../assets//ChooseImg.png";
    // }
    
    this.ELEMENT_DATA = this.ELEMENT_DATA.sort((a, b) => a.Item_Division_Code - b.Item_Division_Code);
    this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource.sort = this.sort;
  }
}
else{
  // this.ELEMENT_DATA.unshift({DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:0
  // ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"});
  // this.ELEMENT_DATA = this.ELEMENT_DATA.sort((a, b) => a.Item_Division_Code - b.Item_Division_Code);
  //   this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
  //   this.dataSource.sort = this.sort;
}
   
  }
  deleterow(index :number){
    console.log(this.ELEMENT_DATA[index].enable);
    if(this.ELEMENT_DATA[index].enable == 1){
     
      this.service.Notificaion('Cannot delete this row', 'Okey')
    }
    else{
      if(this.ELEMENT_DATA.length > 1){
      if(this.ELEMENT_DATA[1].DISPLAY_CARD_ID ==0){
        const data=this.dataSource.data;
        data.splice(index,1);
        this.dataSource.data=data;
      }
      else
      {
        this.service.Notificaion('cannot delete this row', 'okay')
      }
    }
      }
     

  }
  view(){
    this.Viewdata = 1;
    this.fileName = null;
    this.ELEMENT_DATA.length = 0;
    this.ELEMENT_DATA=[{DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:''
    ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"}];
    fd.delete('file');
     this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
     this.GroupId = 0;
  }
  previous(){
    this.Viewdata = null;
    
  }
  submit(){
    
    this.Alength = this.ELEMENT_DATA.length - 1;
 
    // this.ELEMENT_DATA[this.Alength].Listed_Party == "") ||
    console.log(this.fileName);
    
    
  if( (this.ELEMENT_DATA[0].CARD_DISPLAY_NAME == "")
  || (this.ELEMENT_DATA[0].CARD_DISPLAY_DESC == "") || (this.ELEMENT_DATA[0].ITEM_DIVISION_CODE == "") ||
  (this.ELEMENT_DATA[0].LISTED_PARTY_ID == 0) || (this.ELEMENT_DATA[0].CARD_IMAGE_NAME == "") || (this.fileName == null)){
    this.service.Notificaion('Invalid data', 'Okey')
  }
  else{
      fd.append('file',JSON.stringify(this.ELEMENT_DATA));
  
      this.service.postDisplayCard(fd).subscribe(res=>{
        if(res){
          
          if(this.ELEMENT_DATA[0].DISPLAY_CARD_ID == 0){
            this.service.Notificaion('Save sucessfully', 'Okey')
          }
          else{
            this.service.Notificaion('Update sucessfully', 'Okey')
          }
          this.ELEMENT_DATA.length = 0;
          this.ELEMENT_DATA=[{DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:0
          ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"}];
           this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
           this.GroupId = 0
            // this.image[0] = "../../../../assets//ChooseImg.png";
            fd.delete('file');
            this.fileName == null;
            this.service.ViewDisplayCard().subscribe(res=>{
            
              this.ViewDisplay = res
              
              
              this.ViewSource = new MatTableDataSource<PeriodicElement>(this.ViewDisplay);
              
            })
        }
     
      },(err: HttpErrorResponse) => {
       this.service.Notificaion('Enter valid Data', 'okay')
      })
    }
  
  }
  onFileSelected(event: any, index :any) {
    this.fileName = "Files"
    if (event.target.files.length > 0) {
      var reader = new FileReader();
     
        reader.onload = (event: any) => {
        
          
         this.ELEMENT_DATA[index].image = (<FileReader>event.target).result;
        }
        reader.readAsDataURL(event.target.files[0]);
      this.SelectedReecommendedfile[index] = event.target.files[0];
      this.ELEMENT_DATA[index].CARD_IMAGE_NAME = 'http://localhost:49848/'
      
      fd.append('file', this.SelectedReecommendedfile[index]);
      
      // this.ELEMENT_DATA.image[index] = event.target.files[0];
    
      
    
      
      this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
      this.Imagename = this.SelectedReecommendedfile.name
      const file = event.target.files[0];
      // this.ELEMENT_DATA.CARD_IMAGE_NAME = this.SelectedReecommendedfile[index].name;
     
    }
  }
  Filter(event: string) {
    this.ViewSource.filter = event.trim().toLowerCase();
  }
  NameValidate(event: any, index) {
    this.disablebutton = true;
   this.tempdata.length = 0
   this.eachvalidation = this.ELEMENT_DATA.length - 1;
   this.ELEMENT_DATA[index].CARD_DISPLAY_NAME = this.ELEMENT_DATA[index].CARD_DISPLAY_NAME.trim();
   if(this.ELEMENT_DATA[index].CARD_DISPLAY_NAME ==""){
     this.service.Notificaion('Please check card display name is valid', 'okay');
   }
   else{
   this.service.ValidateDisplayName(event.target.value).subscribe(res=>{
  console.log(res);
  
   if(res != []){
     
    this.Displaynamecheck = res[0];
    
    if(this.Displaynamecheck['Column1'] == 'EXISTS'){
      this.ELEMENT_DATA[index].CARD_DISPLAY_NAME = '';
      this.service.Notificaion('Card Display Name is Already Exists', 'okay')
      this.disablebutton = false;
    }
    else{
      this.Displayname = event.target.value
   this.index = index;
      this.CheckCombination();
    }
  }
  })
}
  }
  DescValidate(event:any,index){
    this.disablebutton = true;
    this.ELEMENT_DATA[index].CARD_DISPLAY_DESC = this.ELEMENT_DATA[index].CARD_DISPLAY_DESC.trim();
    if(this.ELEMENT_DATA[index].CARD_DISPLAY_DESC == ""){
      this.service.Notificaion("Please chech Card Discription is valid", "okay");
      this.disablebutton = false;
    }
    else{
      this.disablebutton = false;
    }
  }
  groupids(event:any,index:any): void{
    if(this.GroupId != 0){
    this.ELEMENT_DATA[index].DISPLAY_CARD_GROUP_ID = this.GroupId
    
    }
    else{
      
      this.ELEMENT_DATA=[{DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:0
      ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"}];
      
      
      this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
      this.service.Notificaion('Please select card group', 'okay');
      
      
    }
  }
  edit(event:any){
this.service.EditDisplayCard(event).subscribe(res=>{
  this.Viewdata = null;
  this.GroupId = res[0]['DISPLAY_CARD_GROUP_ID']
   this.ELEMENT_DATA[0] = res[0];
   this.fileName = 'Files';
})
  }
  Groupdata(event:any){
    
    let target = event.source.selected._element.nativeElement;
    let selectedData = {
      value: event.value,
      text: target.innerText.trim()
    };
    this.selectedModuleID = selectedData.value;
    
    this.service.ViewbyGroupid(this.selectedModuleID).subscribe(res=>{
      console.log(res);
      
      this.ELEMENT_DATA = res
 
    this.ELEMENT_DATA.unshift({DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:''
    ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"});
    this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
    })
  
  }
  clear(){
    this.fileName = null;
    this.ELEMENT_DATA.length = 0;
    this.ELEMENT_DATA=[{DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:''
    ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"}];
    fd.delete('file');
     this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
     this.GroupId = 0;
  }
  Indexdata(event:any,index:any): void{
    this.disablebutton = true;
    if(this.GroupId != 0){
      let target = event.source.selected._element.nativeElement;
      let selectedData = {
        value: event.value,
        text: target.innerText.trim()
      };
      this.itemdivisioncode  = selectedData.value;
      this.tempdata.length = 0
   this.eachvalidation = this.ELEMENT_DATA.length - 1;
   
   this.index = index;
   this.CheckCombination();
    
    }
    else{
      
      this.ELEMENT_DATA=[{DISPLAY_CARD_ID:0,Listed_Party:'',CARD_DISPLAY_NAME:'',CARD_DISPLAY_DESC:'',ITEM_DIVISION_CODE:'',Active:false,DISPLAY_CARD_GROUP_ID:this.GroupId,LISTED_PARTY_ID:0
      ,CREATED_BY:1001,CREATION_DATE:this.Date,LAST_UPDATED_BY:'',LAST_UPDATE_DATE:'',LAST_UPDATE_LOGIN:106,CARD_IMAGE_NAME:'',image : "../../../../assets//ChooseImg.png"}];
      
      
      this.dataSource=new MatTableDataSource(this.ELEMENT_DATA);
      this.service.Notificaion('Please select card group', 'okay');
      
      
    }
  }
  CheckCombination(){

    if(this.tempdata.length == 0){
     
      for(let k =0; this.eachvalidation >=k;){
        if((this.ELEMENT_DATA[k].DISPLAY_CARD_ID == 0) && (this.ELEMENT_DATA[k].DISPLAY_CARD_ID != undefined)){
         // this.tempdata = this.ELEMENT_DATA[k];
         this.tempdata.push(this.ELEMENT_DATA[k]);
        
        }
        k++;  
        
      }
      if(this.tempdata.length > 0)
      {
      for(let i= 0; this.tempdata.length - 1 >= i;){
       for (let j=1; this.tempdata.length - 1 >= j; ){
         if(i != j){
          // &&
          // (this.tempdata[i].ITEM_DIVISION_CODE == this.tempdata[j].ITEM_DIVISION_CODE))
          if(this.tempdata[i].CARD_DISPLAY_NAME == this.tempdata[j].CARD_DISPLAY_NAME){
            this.service.Notificaion('this combination already Exists', 'okay');
            this.ELEMENT_DATA[i].CARD_DISPLAY_NAME = '';
            // this.ELEMENT_DATA[i].ITEM_DIVISION_CODE = '';
          }
        }
      
      
    
        j++;
      
       }
      i++;
      
     }
   }
   this.disablebutton = false;
      }
  }
}
