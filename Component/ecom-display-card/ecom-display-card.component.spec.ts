import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECOMDISPLAYCARDComponent } from './ecom-display-card.component';

describe('ECOMDISPLAYCARDComponent', () => {
  let component: ECOMDISPLAYCARDComponent;
  let fixture: ComponentFixture<ECOMDISPLAYCARDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECOMDISPLAYCARDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECOMDISPLAYCARDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
